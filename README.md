# Metadata Security Config Generator #

# Description #

* Quick summary
    This utility can generate the metadata security config from an excel sheet and can extract the 
    metadata security from a tenant into a excel sheet thus maintaining would become easy. 
     
    The User would need to create a excel sheet with all the requirement for the metadata security and 
    the tool would be able to convert into the required JSON that can be posted. 
    Similarly the json of the metadata security could be converted to a excel sheet  

##Change Log
```

  Version           : 1.3.2
  Last Updated Date : 25th Sept 2019
  Last Updated By   : Vignesh Chandran
  Last Changes      : Client Credentials Impl - https://reltio.jira.com/browse/ROCS-84
                      Bug fix - https://reltio.jira.com/browse/ROCS-83
                      Reltio Core CST v1.4.9 updated.

  Version           : 1.3.1
  Last Updated Date : 2nd Jul 2019
  Last Updated By   : Vignesh Chandran
  Last Changes      : Jar Standardization (https://reltio.jira.com/browse/ROCS-48)
                    : Proxy Set up (https://reltio.jira.com/browse/ROCS-47)

  Version           : 1.3.0
  Last Updated Date : 5th March 2019 
  Last Updated By   : Vignesh Chandran
  Last Changes      : Using latest reltio-core-cst 1.4.6; now the password gets encrypted after the first run, standardization of ENVIRONMENT_URL
                      Clear validation message for missing properties

  Version           : 1.2.0
  Last Updated Date : 27th Dec 2018 
  Last Updated By   : Vignesh Chandran
  Last Changes      : Logging Standarization and removal of unnecessary jar

  Version           : 1.1.1
  Last Updated Date : 30 July 2018 
  Last Updated By   : Srikant
  Last Changes      : Standarization of Properties

  Version           : 1.1.0
  Last Updated Date : 30 July 2018 
  Last Updated By   : Vignesh Chandran
  Last Changes      : Reltio Core CST change version 1.4.1

  Version           : 1.0.0
  Last Updated Date : 15 DEC 2017 
  Last Updated By   : Vignesh Chandran
  Last Changes      : Initial Development and Setup
```
##Contributing 
Please visit our [Contributor Covenant Code of Conduct](https://bitbucket.org/reltio-ondemand/common/src/a8e997d2547bf4df9f69bf3e7f2fcefe28d7e551/CodeOfConduct.md?at=master&fileviewer=file-view-default) to learn more about our contribution guidlines

## Licensing
```
Copyright (c) 2017 Reltio

 

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

 

    http://www.apache.org/licenses/LICENSE-2.0

 

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

See the License for the specific language governing permissions and limitations under the License.
```

## Quick Start 
To learn about dependencies, building and executing the tool view our [quick start](https://bitbucket.org/reltio-ondemand/util-metadata-security-service/src/5a057e33d0bb4ccb92768f1b3e373365e892352d/QuickStart.md?at=master&fileviewer=file-view-default).