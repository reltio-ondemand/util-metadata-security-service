# Quick Start 

##Building
* Deployment instructions

    The main method of the application is at the following path:
    com.reltio.metadata.service.MetadataSecurityService
    please run “mvn clean” at the root where the pom.xml file is located and 
    maven will install the library automatically. Then you could run “mvn package” to build the jar file.




##Parameters File Example

```
#Common Properties
TENANT_ID=aO3zrh5wJaqmfGE
ENVIRONMENT_URL=https://sndbx.reltio.com

USERNAME=vignesh.chandran@reltio.com
PASSWORD=*****
or
CLIENT_CREDENTIALS=*******

AUTH_URL=https://auth.reltio.com/oauth/token
HTTP_PROXY_HOST=
HTTP_PROXY_PORT=

#Tool Specific Properties

#Action can be either PUSH or PULL
#[Either to extract the metadata from security from tenant to Excel (PULL) or from Excel to JSON(PUSH)] 
ACTION=PUSH
EXCEL_PATH=~$filePath$/TEST.xlsx
JSON_PATH=$filePath$/test.json


```
##Executing

Command to start the utility.
```
#Windows
java -jar metadata-security-service-1.0.0.jar config.properties > $logfilepath$'

#Unix
java -jar metadata-security-service-1.0.0.jar config.properties > $logfilepath$

```