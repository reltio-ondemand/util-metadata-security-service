package com.reltio.metadata.dto;

import java.util.List;


public class Permission {

    private String role;

    private String filter;

    private List<String> access = null;


    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public List<String> getAccess() {
        return access;
    }

    public void setAccess(List<String> access) {
        this.access = access;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Permission [role=").append(role).append(", filter=").append(filter).append(", access=")
                .append(access).append("]");
        return builder.toString();
    }


}
