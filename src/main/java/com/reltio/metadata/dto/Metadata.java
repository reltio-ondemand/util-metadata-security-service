package com.reltio.metadata.dto;

import java.util.Collections;
import java.util.List;


public class Metadata {

    private String uri;

    private List<Permission> permissions = Collections.emptyList();

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }


    public List<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<Permission> permissions) {
        this.permissions = permissions;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Metadata [uri=").append(uri).append(", permissions=").append(permissions).append("]");
        return builder.toString();
    }
}
