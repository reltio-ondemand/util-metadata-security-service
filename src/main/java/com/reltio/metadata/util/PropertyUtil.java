package com.reltio.metadata.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PropertyUtil {

    private static List<String> usernamePasswordPropertyList = new ArrayList<>(Arrays.asList("USERNAME", "PASSWORD"));
    private static List<String> clientCredentialPropertyList = new ArrayList<>(Collections.singletonList("CLIENT_CREDENTIALS"));
    private static Map<List<String>, List<String>> mutualExclusiveProps;


    public static Map<List<String>, List<String>> getMutualExclusiveProps() {
        mutualExclusiveProps = new HashMap<>();
        mutualExclusiveProps.put(usernamePasswordPropertyList, clientCredentialPropertyList);
        return mutualExclusiveProps;
    }

    public static void setMutualExclusiveProps(Map<List<String>, List<String>> mutualExclusiveProps) {
        PropertyUtil.mutualExclusiveProps = mutualExclusiveProps;
    }
}
