/**
 *
 */
package com.reltio.metadata.util;

import com.reltio.cst.service.ReltioAPIService;
import com.reltio.cst.service.TokenGeneratorService;
import com.reltio.cst.service.impl.SimpleReltioAPIServiceImpl;
import com.reltio.cst.service.impl.TokenGeneratorServiceImpl;

import java.io.FileReader;
import java.util.Collection;
import java.util.Properties;

/**
 * @author spatil
 */
@Deprecated
public abstract class Util {

    public static boolean isEmpty(Collection<?> collection) {

        return collection == null || collection.isEmpty();

    }

    public static boolean isNotEmpty(Collection<?> collection) {

        return collection != null && !collection.isEmpty();

    }


    public static boolean isEmpty(String data) {

        return data == null || data.trim().length() == 0;

    }

    @Deprecated
    public static ReltioAPIService getReltioService(Properties config) throws Exception {

        String username = config.getProperty("USERNAME");
        String password = config.getProperty("PASSWORD");
        String authUrl = config.getProperty("AUTH_URL");

        TokenGeneratorService service = new TokenGeneratorServiceImpl(username, password, authUrl);

        return new SimpleReltioAPIServiceImpl(service);
    }

    public static ReltioAPIService getReltioService(String username, String password, String authUrl) throws Exception {

        TokenGeneratorService service = new TokenGeneratorServiceImpl(username, password, authUrl);

        return new SimpleReltioAPIServiceImpl(service);
    }

    public static Properties getProperties(String propertyFilePath) throws Exception {

        FileReader in = new FileReader(propertyFilePath);
        Properties config = new Properties();
        config.load(in);


        return config;
    }

}
