package com.reltio.metadata.service;

import com.google.gson.Gson;
import com.reltio.cst.exception.handler.APICallFailureException;
import com.reltio.cst.exception.handler.GenericException;
import com.reltio.cst.exception.handler.ReltioAPICallFailureException;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.cst.service.TokenGeneratorService;
import com.reltio.cst.service.impl.SimpleReltioAPIServiceImpl;
import com.reltio.cst.service.impl.TokenGeneratorServiceImpl;
import com.reltio.metadata.dto.Metadata;

import java.util.List;

public class ReltioWriter {

    protected void postToReltio(String hostName, String tenantId, String username, String password, String authUrl,
                                List<Metadata> tgtMetadataSecs)
            throws APICallFailureException, GenericException, ReltioAPICallFailureException {

        Gson GSON = new Gson();
        TokenGeneratorService tokenGeneratorService = new TokenGeneratorServiceImpl(username, password, authUrl);

        ReltioAPIService reltioAPIService = new SimpleReltioAPIServiceImpl(tokenGeneratorService);

        String permissionAPIUrl = hostName + "/reltio/permissions/" + tenantId;


        if (tgtMetadataSecs != null && !tgtMetadataSecs.isEmpty()) {

            String outputReturnResponse = GSON.toJson(tgtMetadataSecs);

            System.out.println("JSON to be send to Reltio : " + outputReturnResponse);

            System.out.println("Deleting the existing configuration...");
            String response = reltioAPIService.delete(permissionAPIUrl, null);
            System.out.println("Deleting done... " + response);

            System.out.println("Posting the new configuration...");
            response = reltioAPIService.post(permissionAPIUrl, outputReturnResponse);
            System.out.println("Posting done... " + response);
        }
    }
}
