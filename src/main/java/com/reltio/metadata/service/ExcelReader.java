package com.reltio.metadata.service;

import com.reltio.metadata.dto.Metadata;
import com.reltio.metadata.dto.Permission;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class ExcelReader {

    public List<List<String>> readFromExcel(FileInputStream excelFile) {
        List<List<String>> table = new LinkedList<>();
        try {
            Workbook workbook = new XSSFWorkbook(excelFile);
            Sheet dataTypeSheet = workbook.getSheetAt(0);
            for (Row currentRow : dataTypeSheet) {
                List<String> row = new LinkedList<>();
                for (Cell currentCell : currentRow) {
                    row.add(currentCell.getStringCellValue());
                }
                table.add(row);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return table;
    }

    public List<Metadata> tableToJson(List<List<String>> table) {

        List<Metadata> metadataSecs = new ArrayList<>();

        List<String> header = table.get(0);
        int count = 0;
        for (List<String> row : table) {
            if (count == 0) {
                count++;
            } else {

                Metadata metadataSec = new Metadata();
                Permission metadataPermissions = new Permission();
                List<Permission> metadataPermissionsList = new ArrayList<>();

                metadataSec.setUri(row.get(0));
                metadataPermissions.setRole(row.get(1));

                List<String> access = Arrays.asList(row.get(2).split("\\|"));
                metadataPermissions.setAccess(access);
                if (row.size() == 4) {
                    metadataPermissions.setFilter(row.get(3));
                }
                metadataPermissionsList.add(metadataPermissions);

                metadataSec.setPermissions(metadataPermissionsList);
                metadataSecs.add(metadataSec);
                count++;
            }


        }

        return metadataSecs;


    }
}



