package com.reltio.metadata.service;

import com.reltio.cst.service.ReltioAPIService;
import com.reltio.cst.util.Util;
import com.reltio.metadata.dto.Metadata;
import com.reltio.metadata.dto.Permission;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import java.util.List;
import java.util.Properties;

public class ExcelWriter {


    public void writeData(XSSFSheet sheet, Metadata[] metadatas) throws Exception {

        int rowNo = 1;

        for (Metadata metadata : metadatas) {
            int column = 0;

            List<Permission> permissions = metadata.getPermissions();

            for (Permission permission : permissions) {

                Row row = sheet.createRow(rowNo++);

                Cell uriCell = row.createCell(column++);
                uriCell.setCellValue(metadata.getUri());

                Cell roleCell = row.createCell(column++);
                roleCell.setCellValue(permission.getRole());

                Cell accessCell = row.createCell(column++);
                if (permission.getAccess() != null) {
                    accessCell.setCellValue(String.join("|", permission.getAccess()));
                }
                Cell filterCell = row.createCell(column++);
                if (permission.getAccess() != null) {
                    filterCell.setCellValue(permission.getFilter());
                }
                column = 0;
            }

        }
    }

    public void writeHeaders(XSSFSheet sheet, Metadata[] metadatas) {

        Row row = sheet.createRow(0);

        Cell cell0 = row.createCell(0);
        cell0.setCellValue("TYPE");

        Cell cell1 = row.createCell(1);
        cell1.setCellValue("ROLE");

        Cell cell2 = row.createCell(2);
        cell2.setCellValue("ACCESS");

        Cell cell3 = row.createCell(3);
        cell3.setCellValue("FILTER");

    }

    /**
     * @param properties
     * @return response
     */

    public String pullMetaDataFromTenant(Properties properties) throws Exception {

        ReltioAPIService api = Util.getReltioService(properties);
        String tenant = properties.getProperty("TENANT_ID");
        String domain = properties.getProperty("ENVIRONMENT_URL");
        return api.get("https://" + domain + "/reltio/permissions/" + tenant);
    }

}
