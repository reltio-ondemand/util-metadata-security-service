package com.reltio.metadata.service;

import com.google.gson.Gson;
import com.reltio.cst.util.Util;
import com.reltio.metadata.dto.Metadata;
import com.reltio.metadata.util.PropertyUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class MetadataSecurityService {


    private static final Logger logger = LogManager.getLogger(MetadataSecurityService.class.getName());

    public static void main(String[] args) throws Exception {

        logger.info("Program Execution Started Started..");

        //check for the args[0] for the path of the config.properties.

        if (args.length == 0) {
            logger.error("Please provide the path for the properties file as a input argument");

        }


        String propertiesFilePath = args[0];
        Properties properties = Util.getProperties(propertiesFilePath, "PASSWORD", "CLIENT_CREDENTIALS");

        List<String> requiredProps = Arrays.asList(
                "TENANT_ID",
                "ENVIRONMENT_URL",
                "ACTION",
                "EXCEL_PATH",
                "JSON_PATH",
                "AUTH_URL");


        List<String> missingProps = Util.listMissingProperties(properties, requiredProps, PropertyUtil.getMutualExclusiveProps());

        if (missingProps.size() > 0) {
            logger.error("Process Aborted due to insufficient input properties... Below are the list of missing properties");
            logger.error(missingProps);

            System.exit(-1);
        }

        if (!Util.isEmpty(properties.getProperty("HTTP_PROXY_HOST")) &&
                !Util.isEmpty(properties.getProperty("HTTP_PROXY_PORT"))) {
            Util.setHttpProxy(properties);
        }
        String action = properties.getProperty("ACTION");

        if (action.equals("PULL")) {
            logger.info("PULL action requested.. Metadata will be pulled and written as Excel File");

            ExcelWriter writer = new ExcelWriter();
            String data = writer.pullMetaDataFromTenant(properties);
            Metadata[] metadataList = new Gson().fromJson(data, Metadata[].class);

            XSSFWorkbook workbook = new XSSFWorkbook();
            XSSFSheet sheet = workbook.createSheet("Metadata Security");
            writer.writeHeaders(sheet, metadataList);
            writer.writeData(sheet, metadataList);
            try {

                String excelFile = properties.getProperty("EXCEL_PATH");

                FileOutputStream outputStream = new FileOutputStream(excelFile);
                workbook.write(outputStream);
                workbook.close();
            } catch (FileNotFoundException e) {
                logger.error("FileNotFoundException" + e.getMessage());
                logger.debug(e);
            } catch (IOException e) {
                logger.error("IOException" + e.getMessage());
                logger.debug(e);
            }
        } else if (action.equals("PUSH")) {
            logger.info("PUSH action requested.. Metadata will be created from the Excel Sheet");
            ExcelReader reader = new ExcelReader();

            String excelFile = properties.getProperty("EXCEL_PATH");
            FileInputStream fileInputStream = new FileInputStream(new File(excelFile));
            Gson gson = new Gson();
            String metadataSecurity = gson.toJson(reader.tableToJson(reader.readFromExcel(fileInputStream)));

            String jsonFile = properties.getProperty("JSON_PATH");

            FileWriter fileWriter = new FileWriter(new File(jsonFile));
            fileWriter.write(metadataSecurity);
            fileWriter.close();
        }

        logger.info("Process Completed...");

    }
}
