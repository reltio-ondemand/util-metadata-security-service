package com.reltio.metadata.service;

import com.google.gson.Gson;
import com.reltio.metadata.dto.Metadata;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class ExcelReaderTest {
    Gson gson = new Gson();

    @Test
    public void readFromExcel() throws FileNotFoundException {

        String filePath = getClass().getResource("/sample.xlsx").getPath();
        FileInputStream excelFile = new FileInputStream(new File(filePath));
        ExcelReader excelReader = new ExcelReader();
        String value1 = excelReader.readFromExcel(excelFile).get(0).get(0);
        Assert.assertEquals("TYPE", value1);

    }

    @Test
    public void tableToJson() throws IOException {

        String filePath = getClass().getResource("/sample.xlsx").getPath();
        FileInputStream excelFile = new FileInputStream(new File(filePath));
        ExcelReader excelReader = new ExcelReader();
        List<Metadata> metadataList = excelReader.tableToJson(excelReader.readFromExcel(excelFile));
        String actualJson = gson.toJson(metadataList);
        System.out.println(actualJson);


    }
}